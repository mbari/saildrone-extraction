// This script reads configuration information from the environment or from a config.json file (environment variables
// take priorty and then pulls location and sensor data from Saildrones.  It sends location information to an AMQP
// server in TrackingDB format and writes sensor data to CSV and NetCDF files.
const config = require('./config')
const axios = require('axios')
const winston = require('winston')
const path = require('path')
const fs = require('fs')
const lineByLine = require('n-readlines')
const amqp = require('amqplib/callback_api')

// ---------------------------------------------------------------------------------------------------------------- //
// Configuration and setup portion of the script
// ---------------------------------------------------------------------------------------------------------------- //

// Read in the configuration from the environment or config file
const apiClientID = process.env.SAILDRONE_API_CLIENT_ID || config.saildroneAPIClientID
const apiClientSecret = process.env.SAILDRONE_API_CLIENT_SECRET || config.saildroneAPIClientSecret
const loggerLevel = (process.env.SAILDRONE_API_LOGGER_LEVEL || config.saildroneAPILoggerLevel) || 'info'
const numHoursBack = (process.env.SAILDRONE_API_NUM_HOURS_BACK || config.saildroneAPINumHoursBack) || 12
const dataPointLimit = (process.env.SAILDRONE_API_DATA_POINT_LIMIT || config.saildroneAPIDataPointLimit) || 1000
let publishToAMQPString = process.env.SAILDRONE_API_PUBLISH_TO_AMQP || config.saildroneAPIPublishToAMQP
let publishToAMQP = true
if (publishToAMQPString === 'false') {
  publishToAMQP = false
}
const amqpHost = (process.env.SAILDRONE_API_AMQP_HOST || config.saildroneAPIAMQPHost) || 'localhost'
const amqpPort = (process.env.SAILDRONE_API_AMQP_PORT || config.saildroneAPIAMQPPort) || '5672'
const amqpUsername = process.env.SAILDRONE_API_AMQP_USERNAME || config.saildroneAPIAMQPUsername
const amqpPassword = process.env.SAILDRONE_API_AMQP_PASSWORD || config.saildroneAPIAMQPPassword
const amqpVHost = process.env.SAILDRONE_API_AMQP_VHOST || config.saildroneAPIAMQPVHost
const amqpExchange = (process.env.SAILDRONE_API_AMQP_EXCHANGE || config.saildroneAPIAMQPExchange) || 'ships'
let writeToCSVString = process.env.SAILDRONE_API_WRITE_TO_CSV || config.saildroneAPIWriteToCSV
let writeToCSV = true
if (writeToCSVString === 'false') {
  writeToCSV = false
}
const localDataDir = (process.env.SAILDRONE_API_LOCAL_DATA_DIR || config.saildroneAPILocalDataDir) || process.cwd()

// Create a logger
const logger = winston.createLogger({
  level: loggerLevel,
  transports: [
    new winston.transports.Console()
  ]
})

// This is the number of active Axios calls
let numOpenAxiosCalls = 0

// ---------------------------------------------------------------------------------------------------------------- //
// Function definitions
// ---------------------------------------------------------------------------------------------------------------- //

// This method reads in the current status and returns it in the form of a JSON object
function getStatus () {
  let status = null
  try {
    status = fs.readFileSync('./status.json', 'utf8')
  } catch (e) {
    logger.warn('No status.json file was found or error trying to read it, OK if this is the first run')
    logger.warn(e)
  }
  if (status != null) {
    status = JSON.parse(status)
  }
  return status
}

// This method takes in a JSON object and writes it to a file to persist the status of the application
function persistStatus (status) {
  // Now update the persist status file to disk
  try {
    fs.writeFileSync('./status.json', JSON.stringify(status))
  } catch (e) {
    logger.error('Error caught trying to persist state of application to disk')
  }
}

// This method bumps a counter to keep track of how many open Axios HTTP calls are still considered open
function addAxiosCall (source) {
  numOpenAxiosCalls++
  logger.debug('Axios call from ' + source + ', bumping up Axios call counts, there are currently ' +
    numOpenAxiosCalls + ' calls open')
}

// This function decrements the Axios counter and if it's zero, it exits the process.  This was needed because the node
// process would not exit after making several nested Axios calls
function decrementAxiosCallsAndExitIfLastOne (source) {
  numOpenAxiosCalls--
  logger.debug('Axios call from ' + source + ' finished, decremented Axios call count, there are ' +
    numOpenAxiosCalls + ' calls still open')
  if (numOpenAxiosCalls <= 0) {
    process.exit(0)
  }
}

// This function takes in the status object, ID of the drone, the data set name, the metadata object, and the array of
// data records to write to a CSV file
function writeDataToCSV (status, droneID, dataSet, metadata, dataRecords, callback) {

  // Make sure we have a drone status object
  if (!status[droneID]) {
    status[droneID] = {
      latestPositionTimestamp: -1
    }
    persistStatus(status)
  }

  // Make sure we have a data set object
  if (!status[droneID][dataSet]) {
    status[droneID][dataSet] = {
      variableOrder: {
        1: 'gps_time',
        2: 'gps_lat',
        3: 'gps_lng'
      },
      variables: {
        'gps_time': {
          index: 1,
          units: 'epoch seconds'
        },
        'gps_lat': {
          index: 2,
          units: 'degrees'
        },
        'gps_lng': {
          index: 3,
          units: 'degrees'
        }
      }
    }
    persistStatus(status)
  }

  // Make sure we have a data records to begin with, otherwise just ignore the call
  if (dataRecords != null) {

    // Now with the saildrone, you often get incomplete records for the most recent records, so that means we will over-
    // write any of the data in the file that we have in the data records given in this call if a file already exists
    // Check if base data directory exists
    if (!fs.existsSync(localDataDir)) {
      try {
        fs.mkdirSync(localDataDir)
      } catch (e) {
        logger.error('Error caught trying to create base data directory')
        logger.error(e)
      }
    }

    // Now make sure we have the directory to where the drone data will go
    let droneDataPath = null
    try {
      droneDataPath = path.join(localDataDir, droneID.toString())
      if (!fs.existsSync(droneDataPath)) {
        fs.mkdirSync(droneDataPath)
      }
    } catch (e) {
      logger.error('Error trying to create a path to data file for drone with ID ' + droneID)
      logger.error(localDataDir + '/' + droneID)
      logger.error(e)
    }

    // Now create the path to the data file itself
    let csvFile = null
    try {
      csvFile = path.join(localDataDir, droneID.toString(), dataSet + '.csv')
    } catch (e) {
      logger.error('Error caught trying to create data file ' + dataSet + '.csv')
      logger.error(e)
    }

    // Now if the file exists, let's move it to a backup directory
    let backupCSVFile = null
    if (fs.existsSync(csvFile)) {
      // Make sure we have a backup directory
      let backupPath = null
      try {
        backupPath = path.join(localDataDir, droneID.toString(), 'backup')
        if (!fs.existsSync(backupPath)) {
          fs.mkdirSync(backupPath)
        }
      } catch (e) {
        logger.error('Error trapped trying to create backup directory')
      }

      // Move the file
      try {
        backupCSVFile = path.join(localDataDir, droneID.toString(), 'backup', dataSet + '-' +
          Math.floor(new Date() / 1000) + '.csv')
        fs.renameSync(csvFile, backupCSVFile)
      } catch (e) {
        logger.error('Error caught trying to move CSV file to backup')
        logger.error(e)
      }
    }

    // Let's first loop over the data records and make sure we have all the variables in the variable map
    dataRecords.forEach((dataRecord) => {
      //logger.debug('Processing ' + dataSet + ' record at ' + dataRecord['gps_time'])

      // The first thing we need to do, is look at the data record and make sure all the variables are already tracked
      // in the data set status

      // Grab the current number of keys in the variable order property of the data set
      let counter = Object.keys(status[droneID][dataSet]['variableOrder']).length
      for (let key in dataRecord) {
        // Make sure it's instance property
        if (dataRecord.hasOwnProperty(key)) {
          // See if it's in the variable list
          if (!status[droneID][dataSet]['variables'][key]) {
            // Bump the counter
            counter++
            // Add it to the status
            status[droneID][dataSet]['variables'][key] = {
              index: counter
            }
            // If units are in metadata, add those too
            if (metadata['units'] && metadata['units'][key] && metadata['units'][key]['unit_name']) {
              status[droneID][dataSet]['variables'][key]['units'] = metadata['units'][key]['unit_name']
            }

            // Add they key to the index lookup
            status[droneID][dataSet]['variableOrder'][counter] = key

            // And persist it
            persistStatus(status)
          }
        }
      }

    })

    // Now that the variable map is up to speed, write the header to file
    if (!fs.existsSync(csvFile)) {
      // Now iterate over the variable order
      let headerRow = 'time (epoch seconds),latitude (degrees),longitude (degrees)'
      for (let dataRecordIndex = 4; dataRecordIndex < Object.keys(status[droneID][dataSet]['variableOrder']).length + 1; dataRecordIndex++) {
        // Add the name
        headerRow += ',' + status[droneID][dataSet]['variableOrder'][dataRecordIndex]

        // If units are found, add those too
        if (status[droneID][dataSet]['variables'][status[droneID][dataSet]['variableOrder'][dataRecordIndex]]['units']) {
          headerRow += '(' +
            status[droneID][dataSet]['variables'][status[droneID][dataSet]['variableOrder'][dataRecordIndex]]['units'] + ')'
        }
      }

      // Now we have the header row, write it to the file
      fs.writeFileSync(csvFile, headerRow + '\n')
    }

    // Grab the earliest timestamp from the incoming data array
    let earliestTimestamp = dataRecords[0]['gps_time']
    logger.debug('Earliest timestamp in incoming data is ' + earliestTimestamp)
    // TODO kgomes, read rows from the old file and write them to the new file if they are before the first timestamp

    // If there is a backup file, we want to read in all the rows that are before the earliest timestamp in this query
    if (backupCSVFile) {
      // Create the line reader
      const liner = new lineByLine(backupCSVFile)

      // Create a handle to the line being read
      let line

      try {
        // Loop through the file line by line
        while (line = liner.next()) {
          // Convert the line to a string
          let lineTimestamp = Number.parseInt(line.toString().split(',')[0])
          if (lineTimestamp && lineTimestamp < earliestTimestamp) {
            fs.appendFileSync(csvFile, line.toString() + '\n')
          }
        }
        // Now remove the file
        fs.unlinkSync(backupCSVFile)
      } catch (e) {
        logger.error('Error caught trying to write earlier data to new file')
        logger.error(e)
      }

    }

    // Now loop over data records again
    dataRecords.forEach((dataRecord) => {

      // Since no new data was in the incoming record, just append it to the file. To do this we loop over the
      // variables in the status for the data set and look for each variable in the record
      let dataRow = dataRecord['gps_time'] + ',' + dataRecord['gps_lat'] + ',' + dataRecord['gps_lng']

      // Iterate over variables we have in our little local catalog
      for (let dataRecordIndex = 4; dataRecordIndex < Object.keys(status[droneID][dataSet]['variableOrder']).length + 1; dataRecordIndex++) {
        // Grab the name of the data item from the data set order object
        let dataItemName = status[droneID][dataSet]['variableOrder'][dataRecordIndex]

        // Make sure there is a value
        if (dataRecord[dataItemName]) {
          // We need to watch out for arrays as they need to be enclosed in quotes
          if (Array.isArray(dataRecord[dataItemName])) {
            dataRow += ',"' + dataRecord[dataItemName] + '"'
          } else {
            // Now grab the data itself from the record and append
            dataRow += ',' + dataRecord[dataItemName]
          }
        } else {
          // Now grab the data itself from the record and append
          dataRow += ','
        }
      }
      fs.appendFileSync(csvFile, dataRow + '\n')
    })

    // All done, call the given callback function
    callback()
  } else {
    // Incoming data record set was null, will do nothing
    logger.debug('Incoming data records were null, will do nothing')
    callback()
  }
}

// This function takes in a status object, an AMQP channel, a drone ID and an array of data and if there are vehicle
// positions in the data that are more recent than the latest recorded in the status object, publish them to AQMP
function publishLocationsToAMQP (status, amqpChannel, droneID, data, callback) {
  // Make sure there is data in the incoming array
  if (data && data.length > 0) {
    // Iterate over each data point
    let errorToReturn = null
    data.forEach((dataItem) => {
      // Grab lat, lon and time
      let lat = dataItem['gps_lat']
      let lon = dataItem['gps_lng']
      let timestamp = dataItem['gps_time']

      // First check to see if there is a status object for the drone with the current ID
      if (status[droneID] == null) {
        status[droneID] = {
          latestPositionTimestamp: -1
        }
        // Update the drone status
        persistStatus(status)
      }

      // Check to see if the timestamp is more recent than the last timestamp in the status
      if (!status[droneID].latestPositionTimestamp || status[droneID].latestPositionTimestamp < timestamp) {
        // Create a timestamp with the timestamp on the data record
        let tsDate = new Date(0)
        tsDate.setUTCSeconds(timestamp)

        // Create the message
        let message = 'ship,sd' + droneID + ',' + timestamp + ',' + lat + ',' + lon + ',sdextract,,,' +
          tsDate.toISOString()
        logger.debug('Drone ' + droneID + ' position is new: ' + message)
        try {
          amqpChannel.publish(amqpExchange, '', Buffer.from(message))
        } catch (e) {
          logger.error('Error trying to publish message')
          logger.error(e)
          errorToReturn = e
        }
        status[droneID].latestPositionTimestamp = timestamp

        // Update the drone status
        persistStatus(status)
      }
    })
    callback(errorToReturn)
  } else {
    logger.warn('In publishLocationsToAMQP, the incoming data set array is empty for drone' + droneID)
    callback()
  }
}

// This method takes in an access token, status object, AMQP channel, drone object and a data set name that needs to
// be processed
function processDroneDataSet (accessToken, status, amqpChannel, droneID, dataSet, callback) {
  logger.debug('Working with drone ' + droneID + ' data set ' + dataSet)

  // Grab the current time
  let endDate = new Date()
  let endTimestamp = endDate.getTime() / 1000

  // Create a timestamp from 12 hours ago
  let startTimestamp = endTimestamp - (numHoursBack * 60 * 60)
  let startDate = new Date(0)
  startDate.setUTCSeconds(startTimestamp)

  // Now create the URL that will be called to get the drone data
  let dataSetUrl = 'https://developer-mission.saildrone.com/v1/timeseries/' + droneID +
    '?data_set=' + dataSet +
    '&start_date=' + startDate.toISOString() +
    '&end_date=' + endDate.toISOString() +
    '&order_by=asc&limit=' + dataPointLimit
  logger.debug('will call URL ' + dataSetUrl)

  // Bump the call counter and make the URL call to get the data
  addAxiosCall('getDataSetDrone' + droneID + ':' + dataSet)
  axios({
    method: 'get',
    url: dataSetUrl,
    headers: {
      'authorization': accessToken
    }
  }).then((response) => {
    logger.debug('Returned from call to get data set ' + dataSet)
    // Make sure we got a response
    if (response && response.data) {

      // Grab the metadata from the response
      let metadata = response.data.meta

      // Grab the data array
      let dataRecords = response.data.data

      // Now if we are publishing locations to AMQP, call that function
      if (publishToAMQP) {
        publishLocationsToAMQP(status, amqpChannel, droneID, dataRecords, function (error) {
          logger.debug('Done publishing messages to AMQP')
          if (error) {
            logger.error('Error calling publishLocationsToAMQP')
            logger.error(error)
          }
          if (writeToCSV) {
            writeDataToCSV(status, droneID, dataSet, metadata, dataRecords, function (error) {
              callback(error)
              decrementAxiosCallsAndExitIfLastOne('getDataSetDrone' + droneID + ':' + dataSet)
            })
          } else {
            callback()
            decrementAxiosCallsAndExitIfLastOne('getDataSetDrone' + droneID + ':' + dataSet)
          }
        })
      } else {
        logger.debug('Publishing to AMQP turned off')
        if (writeToCSV) {
          writeDataToCSV(status, droneID, dataSet, metadata, dataRecords, function (error) {
            callback(error)
            decrementAxiosCallsAndExitIfLastOne('getDataSetDrone' + droneID + ':' + dataSet)
          })
        } else {
          callback()
          decrementAxiosCallsAndExitIfLastOne('getDataSetDrone' + droneID + ':' + dataSet)
        }
      }
    } else {
      logger.warn('No response from query for data set ' + dataSet + ' from drone ' + droneID)
      callback()
      decrementAxiosCallsAndExitIfLastOne('getDataSetDrone' + droneID + ':' + dataSet)
    }
  }).catch((error) => {
    logger.error('Error caught trying to get data set for ' + dataSet + ' from drone ' + droneID)
    logger.error(error)
    callback(error)
    decrementAxiosCallsAndExitIfLastOne('getDataSetDrone' + droneID + ':' + dataSet)
  })
}

// This function takes in an access token, status object, AMQP channel and a drone information object and processes
// data for that drone
function processDroneData (accessToken, status, amqpChannel, drone, callback) {
  logger.debug('Processing data from drone with ID ' + drone['drone_id'])
  let droneID = drone['drone_id']

  // Make sure there is a data set array
  if (drone && drone['data_set'] && drone['data_set'].length > 0) {
    // Grab the data sets array
    let dataSets = drone['data_set']

    // Make sure there are data sets
    if (dataSets.length > 0) {
      // Now loop over each data set
      let dataSetCounter = 0
      dataSets.forEach((dataSet) => {
        dataSetCounter++
        logger.debug('Calling processDroneDataSet for drone ' + droneID + ' data set ' + dataSet)
        processDroneDataSet(accessToken, status, amqpChannel, droneID, dataSet, function (error) {
          if (error) {
            logger.error('Error caught trying to call processDroneDataSet')
            logger.error(error)
          }
          dataSetCounter--
          logger.debug('Back from processDroneDataSet call for ' + droneID + ' data set ' + dataSet +
            ' and there are ' + dataSetCounter + ' calls left')
          if (dataSetCounter <= 0) {
            callback()
          }
        })
      })
    } else {
      logger.debug('No data sets in drone definition object. This is weird')
      callback()
    }
  } else {
    logger.warn('No data sets listed for drone with ID ' + droneID)
    callback()
  }
}

// This function takes in an access token, a status object and an AMQP channel and then uses all that to query for
// saildrone data and then send positions to the AMQP server and processes the data into CSV files
function processAllDroneData (accessToken, status, amqpChannel, callback) {
  // Make a call to get the drone array
  addAxiosCall('getDroneList')
  axios({
    method: 'get',
    url: 'https://developer-mission.saildrone.com/v1/auth/access',
    headers: {
      'authorization': accessToken
    }
  }).then((response) => {
    // Makes sure we have some results
    if (response && response.data && response.data.data && response.data.data.access) {
      logger.debug('Drone array found with ' + response.data.data.access.length + ' elements')
      // Grab the array of drones
      let droneArray = response.data.data.access

      // If there are any process them
      if (droneArray && droneArray.length > 0) {

        // Loop over each drone and process the data from that drone
        let droneCounter = 0
        droneArray.forEach((drone) => {
          droneCounter++
          logger.debug('Calling process drone data for the ' + droneCounter + ' time')
          processDroneData(accessToken, status, amqpChannel, drone, function (error) {
            if (error) {
              logger.error('Error caught trying to call processDroneData')
              logger.error(error)
            }
            droneCounter--
            logger.debug('Returned from processDroneData call, there are ' + droneCounter +
              ' calls left to return')
            if (droneCounter <= 0) {
              callback()
              decrementAxiosCallsAndExitIfLastOne('getDroneList')
            }
          })
        })
      } else {
        logger.debug('No drones returned in call to get drone list')
        callback()
        decrementAxiosCallsAndExitIfLastOne('getDroneList')
      }
    } else {
      logger.debug('Does not look like there is any drone array list')
      callback()
      decrementAxiosCallsAndExitIfLastOne('getDroneList')
    }
  }).catch((error) => {
    logger.error('Error caught trying to get list of drones')
    logger.error(error)
    callback(error)
    decrementAxiosCallsAndExitIfLastOne('getDroneList')
  })
}

// This function connects to the AMQP server and hands a newly minted channel to the next function which processes all
// data from the saildrones
function connectToAMQPAndProcessDroneData (accessToken, status, callback) {
  // Construct the URL to the AMQP server
  let amqpUrl = 'amqp://'
  if (amqpUsername && amqpPassword) {
    amqpUrl += amqpUsername + ':' + amqpPassword + '@'
  }
  amqpUrl += amqpHost
  if (amqpPort) {
    amqpUrl += ':' + amqpPort
  }
  if (amqpVHost) {
    amqpUrl += '/' + amqpVHost
  }

  // Now connect to the server
  amqp.connect(amqpUrl, function (error, connection) {
    if (error) {
      logger.error('Error trying to connect to the AQMP server')
      logger.error(error)
      callback(error)
    } else {
      logger.debug('Connected to AMQP successfully')
      // Try to create a channel
      connection.createChannel(function (error1, channel) {
        if (error1) {
          logger.error('Error trying to create a channel')
          logger.error(error1)
          callback(error1)
        } else {
          logger.debug('AMQP Channel created successfully')
          // Call the function to process drone data with this AMQP channel
          processAllDroneData(accessToken, status, channel, function (error2) {
            if (error2) {
              logger.error('Error caught trying to call processAllDroneData')
              logger.error(error2)
            }
            logger.debug('In callback from processAllDroneData call')
            callback(error2)
          })
        }
      })
    }
  })
}

// ---------------------------------------------------------------------------------------------------------------- //
// Main executable portion of the script
// ---------------------------------------------------------------------------------------------------------------- //
logger.info('Starting Saildrone extraction script')

// Let's try to read in the JSON file where the progress data is being kept
let status = getStatus()

// If nothing was read in, create an empty JSON object and write it to the file
if (status == null) {
  status = {}
  persistStatus(status)
}

// Let's see if we have an access token in the status file
let accessToken = status.accessToken
if (accessToken == null) {
  logger.debug('Looks like we do not have an access token yet, will call the API to get one')
  // Since we don't we will need to call the API and try to grab the access token
  addAxiosCall('getAccessToken')
  axios({
    method: 'post',
    url: 'https://developer-mission.saildrone.com/v1/auth',
    data: {
      key: apiClientID,
      secret: apiClientSecret
    }
  }).then((response) => {
    // Make sure there is a response with data
    if (response && response.data) {
      // Make sure a token was found
      if (response.data.token) {
        logger.debug('Found an access token')
        // Add the token to the persistence
        accessToken = response.data.token
        status.accessToken = accessToken
        persistStatus(status)

        // Now call function to process drone data for all drones associated with the access token
        connectToAMQPAndProcessDroneData(accessToken, status, function (error) {
          if (error) {
            logger.error('Error caught trying to call connectToAMQPAndProcessDroneData')
            logger.error(error)
          }
          logger.debug('All done with connectToAMQPAndProcessDroneData call')
          decrementAxiosCallsAndExitIfLastOne('getAccessToken')
        })
      } else {
        logger.warn('Access token request came back with no token in the response')
        decrementAxiosCallsAndExitIfLastOne('getAccessToken')
      }
    } else {
      logger.warn('Trying to get Access Token, but response and/or response data was empty')
      decrementAxiosCallsAndExitIfLastOne('getAccessToken')
    }
  }).catch((error) => {
    logger.error('Error caught trying to get access token from the API: ')
    logger.error(error)
    decrementAxiosCallsAndExitIfLastOne('getAccessToken')
  })
} else {
  logger.debug('There is an existing access token, will use that')
  // Call the method to process drone data for all drones associated with the access token
  connectToAMQPAndProcessDroneData(accessToken, status, function (error) {
    if (error) {
      logger.error('Error caught trying to call connectToAMQPAndProcessDroneData')
      logger.error(error)
    }
    logger.debug('All done with connectToAMQPAndProcessDroneData call')
  })
}

