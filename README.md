# Saildrone Data Extraction

This code can be used to extract data from the Saildrone REST API and then publish location messages to an AMQP server and/or write data files in CSV format to a directory somewhere.  In order to run this code, you need at least Node JS 6.  The configuration for this script can be done in one of two ways, by environment variables or by a file located in the same directory as the index.js file named config.json. The properties are:

| Environment Variable | Config.json property | Description | Required? | Default |
| ---------------------| ---------------------| ----------- | --------- | ------- |
| SAILDRONE_API_CLIENT_ID | saildroneAPIClientID | This is an ID provided by Saildrone that is used to get an access token | Y | |
| SAILDRONE_API_CLIENT_SECRET | saildroneAPIClientSecret | This is a secret that is used in combination with the client ID to get an access token | Y | |
| SAILDRONE_API_LOGGER_LEVEL | saildroneAPILoggerLevel | Adjusts the level of output to the console. Basically you can choose between 'info' and 'debug' | N | info |
| SAILDRONE_API_PUBLISH_TO_AMQP | saildroneAPIPublishToAMQP | A true/false flag to indicate if the script should send messages to an AMQP server | N | true |
| SAILDRONE_API_AMQP_HOST | saildroneAPIAMQPHost | The hostname of the AMQP server where messages will be sent | Y | localhost |
| SAILDRONE_API_AMQP_PORT | saildroneAPIAMQPPort | The port where the AMQP server is listenting | N | 5672 |
| SAILDRONE_API_AMQP_USERNAME | saildroneAPIAMQPUsername | The username used to connect to the AMQP server | N | |
| SAILDRONE_API_AMQP_PASSWORD | saildroneAPIAMQPPassword | The password used to connect to the AMQP server | N | |
| SAILDRONE_API_AMQP_VHOST | saildroneAPIAMQPVHost | The VHost on the AQMP server where messages will be sent | Y | |
| SAILDRONE_API_AMQP_EXCHANGE | saildroneAPIAMQPExchange | The name of the exchange where message will be sent. Defaults to ships as that is most likely when sending message to MBARI's tracking database | Y | ships |
| SAILDRONE_API_WRITE_TO_CSV | saildroneAPIWriteToCSV | A true/false flag to indicate if the script should write the extracted data to CSV files. | N | true |
| SAILDRONE_API_LOCAL_DATA_DIR | saildroneAPILocalDataDir | The directory where the CSV data files will be written | N | Your current working directory | 
| SAILDRONE_API_NUM_HOURS_BACK | saildroneAPINumHoursBack | The number of hours back in time (from the time when the script is run) to query for data | N | 12 |
| SAILDRONE_API_DATA_POINT_LIMIT | saildroneAPIDataPointLimit | The number of points to limit the return to | N | 1000 |

Once the configuration is complete, the script can be run using

        npm install
        node index.js

Note that the script will use a file named status.json to track where things were left off the last time it ran.  You should leave this file untouched unless you want to clear any memory of where things left off and just run it like you did the first time. There is a script that can be used to run as cron named 'run-sd-extract.sh' and you just need to define the environment variables in the script before running it.
